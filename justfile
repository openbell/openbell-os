#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

default:
    just --list --unsorted

# Build OpenBell OS with the given `STREAM`
build VARIANT STREAM:
    #!/bin/bash
    set -euo pipefail

    just validate_stream {{STREAM}}
    just validate_variant {{VARIANT}}

    if [ "{{VARIANT}}" = "hub" ]; then
        export KAS_TARGET="openbell-image-hub"
    elif [ "{{VARIANT}}" = "doorcam" ]; then
        export KAS_TARGET="openbell-image-doorcam"
    fi

    kas build kas/{{STREAM}}.yml

# Cleans all OpenBell components
clean:
    #!/bin/bash
    set -euo pipefail
    kas shell kas/base.yml -c "bitbake -c cleansstate \
        openbell-server \
        openbell-doorcam \
        openbell-hub \
        openbell-hub-rust-lib"

[private]
validate_stream STREAM:
    #!/bin/bash
    set -euo pipefail

    case "{{STREAM}}" in
      dev|nightly|staging|stable)
        # Valid stream
        ;;
      *)
        echo "Invalid STREAM: {{STREAM}}. Valid options are: dev, nightly, staging, stable" >&2
        exit 1
        ;;
    esac

[private]
validate_variant VARIANT:
    #!/bin/bash
    set -euo pipefail

    case "{{VARIANT}}" in
      hub|doorcam)
        # valid
        ;;
      *)
        echo "Invalid VARIANT: {{VARIANT}}. Valid options are: hub, doorcam." >&2
        exit 1
        ;;
    esac
