#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "Flutter Rust Bridge"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=480e9b5af92d888295493a5cc7f2238e"

SRC_URI = "git://github.com/fzyzcjy/flutter_rust_bridge.git;protocol=https;branch=master"
SRCREV = "5dead4fc73a537e42673242eb47543287a696eeb"
S = "${WORKDIR}/git"

inherit cargo_bin

do_compile[network] = "1"

BBCLASSEXTEND = "native"

do_compile:append:class-native() {
    cargo install cargo-expand --root ${B}
}

do_install:append:class-native(){
    install -m 0755 ${B}/bin/cargo-expand ${D}${bindir}
}
