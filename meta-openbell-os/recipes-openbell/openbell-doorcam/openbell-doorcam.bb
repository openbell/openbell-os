#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "OpenBell DoorCam"
LICENSE = "AGPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=539fd8525416c2e199f44362a6706898"

BRANCH ?= "${OPENBELL_BRANCH}"
SRC_URI = "git://gitlab.com/openbell/openbell-doorcam.git;protocol=https;branch=${BRANCH}"
SRC_URI:append = " file://openbell-doorcam.service"
SRCREV = "fa57356a3115aec136dacdb658e3bd0a9b7fc8d4"
S = "${WORKDIR}/git"

inherit cargo_bin systemd pkgconfig

DEPENDS += "cmake-native clang-native dbus libevent libcamera"
RDEPENDS:${PN} += "systemd"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "openbell-doorcam.service"

INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP:${PN} += "already-stripped"

do_compile[network] = "1"

do_compile:prepend() {
    # Export C++ STD header paths.
    # This is used by OpenBell DoorCam for building the C++ camera lib.
    compiler_includes=`${CXX} -xc++ /dev/null -E -Wp,-v 2>&1 | sed -n 's,^ ,,p'`
    export CROSS_COMPILE_INCLUDES="`echo ${compiler_includes} | tr '\n' ' '`${STAGING_INCDIR}/libcamera"
}

do_install:append() {
    # Install OpenBell DoorCam base config
    install -d ${D}${sysconfdir}/openbell-doorcam
    install -m 0644 ${S}/configs/config.toml ${D}${sysconfdir}/openbell-doorcam/config.toml

    # Install systemd service file
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/openbell-doorcam.service ${D}${systemd_system_unitdir}/openbell-doorcam.service
}
