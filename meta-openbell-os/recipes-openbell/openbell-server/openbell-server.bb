#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "OpenBell Server"
LICENSE = "AGPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=539fd8525416c2e199f44362a6706898"

BRANCH ?= "${OPENBELL_BRANCH}"
SRC_URI = "git://gitlab.com/openbell/openbell-server.git;protocol=https;branch=${BRANCH}"
SRC_URI:append = " file://openbell-server.service"
SRCREV = "235962d768a492e48bd2a41074babd13b912cab1"
S = "${WORKDIR}/git"

inherit cargo_bin systemd

DEPENDS += "sqlite3"
RDEPENDS:${PN} += "systemd"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "openbell-server.service"

INHIBIT_PACKAGE_STRIP = "1"
INSANE_SKIP:${PN} += "already-stripped"

do_compile[network] = "1"

do_install:append() {
    # Install OpenBell Server base config
    install -d ${D}${sysconfdir}/openbell-server
    install -m 0644 ${S}/config/config.toml ${D}${sysconfdir}/openbell-server/config.toml

    # Install systemd service file
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/openbell-server.service ${D}${systemd_system_unitdir}/openbell-server.service
}
