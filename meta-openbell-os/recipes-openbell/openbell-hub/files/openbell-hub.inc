[Unit]
Description=Launch OpenBell Hub
After=network.target

[Service]
Environment="RUST_LOG=debug"
Environment="CONFIG_DIR=/etc/openbell-hub"
Environment="DATA_DIR=/var/lib/openbell-hub"
WorkingDirectory=/usr/share/openbell_hub/@FLUTTER_SDK_VERSION@/release/
ExecStart=flutter-pi --release /usr/share/openbell_hub/@FLUTTER_SDK_VERSION@/release/
Restart=on-failure
User=obhub
Environment=DISPLAY=:0

[Install]
WantedBy=multi-user.target
