#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "OpenBell Hub User"
DESCRIPTION = "Creates a user for the OpenBell Hub service"
LICENSE = "Apache-2.0"

inherit useradd

DEPENDS += "libdrm"
RDEPENDS:${PN} += "libdrm"

GROUPADD_PARAM:${PN} = "render"
USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "-u 1001 -d /home/obhub -r -s /bin/false -G audio,video,input,render obhub"

do_install:append() {
    install -d -m 0755 ${D}/home/obhub
}

FILES:${PN} += "/home/obhub"
