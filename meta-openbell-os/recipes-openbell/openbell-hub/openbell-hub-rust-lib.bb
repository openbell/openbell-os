#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "OpenBell Hub Rust Lib"
LICENSE = "AGPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=539fd8525416c2e199f44362a6706898"

BRANCH ?= "${OPENBELL_BRANCH}"
SRC_URI = "git://gitlab.com/openbell/openbell-hub.git;protocol=https;branch=${BRANCH}"
SRCREV = "75682d0a27a88d2d9701b06276fffcd5251af101"
S = "${WORKDIR}/git"

inherit cargo_bin

DEPENDS += "flutter-sdk-native flutter-rust-bridge-native dbus alsa-lib clang-native"

INSANE_SKIP:${PN} += " already-stripped ldflags"
INHIBIT_PACKAGE_STRIP = "1"
SOLIBS = ".so"
FILES_SOLIBSDEV = ""

do_configure[network] = "1"
do_compile[network] = "1"

export PATH := "${STAGING_DATADIR_NATIVE}/flutter/sdk/bin:${PATH}"
export CARGO_BUILD_TARGET = "aarch64-unknown-linux-gnu"

do_configure:append() {
    # Create a symbolic link from clang to cc to prevent flutter_rust_bridge_codegen from failing
    ln -sf "${STAGING_BINDIR_NATIVE}/clang" "${STAGING_BINDIR_NATIVE}/cc"

    cd ${S}
    flutter_rust_bridge_codegen generate
}

do_install:append() {
    # Install generated Rust source code into a data directory
    install -d ${D}${datadir}/openbell/lib_generated
    cp -r ${S}/lib/generated/* ${D}${datadir}/openbell/lib_generated/

    install -d ${D}${datadir}/openbell/rust_generated
    cp -r ${S}/rust/src/generated/* ${D}${datadir}/openbell/rust_generated/
}

FILES:${PN}-dev += "${datadir}/openbell/lib_generated/* ${datadir}/openbell/rust_generated/*"
