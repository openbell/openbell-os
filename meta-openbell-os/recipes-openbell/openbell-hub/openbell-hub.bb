#-----------------------------------------------------------------------------------------------------------------------
# Copyright 2025 Kyle Finlay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-----------------------------------------------------------------------------------------------------------------------

SUMMARY = "OpenBell Hub"
LICENSE = "AGPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=539fd8525416c2e199f44362a6706898"

BRANCH ?= "${OPENBELL_BRANCH}"
SRC_URI = "git://gitlab.com/openbell/openbell-hub.git;protocol=https;branch=${BRANCH}"
SRC_URI:append = " file://0001-remove-rust-lib-dependency.patch"
SRC_URI:append = " file://openbell-hub.inc"
SRCREV = "0327d5f4e9fa43333ee7640221b6921d689a9de2"
S = "${WORKDIR}/git"

inherit flutter-app systemd

PUBSPEC_APPNAME = "openbell_hub"
FLUTTER_APPLICATION_INSTALL_PREFIX = "${datadir}"
FLUTTER_APPLICATION_INSTALL_SUFFIX = "openbell_hub"

DEPENDS:append = " openbell-hub-rust-lib openbell-hub-user"
RDEPENDS:${PN} += " openbell-hub-rust-lib openbell-hub-user"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "openbell-hub.service"

do_configure[network] = "1"
do_fetch_dart_dependencies[network] = "1"
do_compile[network] = "1"

export PATH := "${STAGING_DIR_NATIVE}/usr/share/flutter/sdk/bin:${PATH}"

FILES:${PN} += "${sysconfdir}/openbell-hub/*"
FILES:${PN} += "/var/lib/openbell-hub"

do_copy_flutter_gen_files() {
    cp -r ${STAGING_DATADIR}/openbell/lib_generated/* ${S}/lib/generated/
}

do_fetch_dart_dependencies() {
    cd ${S}
    PUB_CACHE="${PUB_CACHE}" flutter pub get
}

do_install:append() {
    install -d ${D}/var/lib/openbell-hub
    chown obhub:obhub ${D}/var/lib/openbell-hub
    chmod 0755 ${D}/var/lib/openbell-hub

    install -d ${D}${sysconfdir}/openbell-hub
    install -m 0644 ${S}/configs/config.toml ${D}${sysconfdir}/openbell-hub/config.toml

    # Replace "@FLUTTER_SDK_VERSION@" with the actual flutter version in the systemd service file.
    sed "s|@FLUTTER_SDK_VERSION@|${FLUTTER_SDK_VERSION}|g" \
        ${WORKDIR}/openbell-hub.inc > ${WORKDIR}/openbell-hub.service

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/openbell-hub.service ${D}${systemd_system_unitdir}/openbell-hub.service
}

addtask do_copy_flutter_gen_files after do_configure before do_compile
addtask do_fetch_dart_dependencies after do_copy_flutter_gen_files before do_compile
