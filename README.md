# OpenBell OS

OpenBell OS is a Yocto-based operating system for the OpenBell smart doorbell project.

## Requirements

* Linux-based system (Ubuntu, Fedora, openSUSE, etc.)
* [Yocto Project dependencies](https://docs.yoctoproject.org/ref-manual/system-requirements.html#required-packages-for-the-build-host)
* [KAS build tool](https://kas.readthedocs.io/en/latest)
* [Just](https://github.com/casey/just)
* Git

## Building

The project uses the [just](https://github.com/casey/just) command runner for build operations.  \
The system can be built in different variants (hub/doorcam) and for different streams (dev/nightly/staging/stable).

### Basic Build Commands

```bash
# Build the `Hub` variant for development
just build hub dev

# Build the `DoorCam` variant for stable
just build doorcam stable
```

### Available Variants

* `hub`: Includes the [OpenBell Hub](https://gitlab.com/openbell/openbell-hub) and [OpenBell Server](https://gitlab.com/openbell/openbell-server) software for running on OpenBell Hub devices
* `doorcam`: Includes the [OpenBell DoorCam](https://gitlab.com/openbell/openbell-doorcam) software for OpenBell DoorCam devices

### Available streams

* `dev`: Development build (for active development)
* `nightly`: Nightly build with latest features
* `staging`: Pre-releasing testing build
* `stable`: Stable release build

> **PLEASE NOTE**: The `dev` stream enables passwordless root login.  \
> *DO NOT* use it for deployed devices. If you want the latest bugs and features, use nightly instead.

### Cleaning the build

To clean all OpenBell components:

```bash
just clean
```

This is usefull for performing a clean build for OpenBell Server, Hub, and DoorCam.

## Hardware Support

OpenBell OS is designed for the Raspberry Pi Zero 2 W, and is guaranteed to work on it.  \
It may also work on other members of the Raspberry Pi family, however this has not been tested.


## License

OpenBell Server, Hub, and DoorCam are licensed under the AGPL-3.0 license.  \
See the LICENSE file in each component repository for more information.

The Yocto build system configuration and meta-layer are licensed under the Apache License, Version 2.0.

## Contributing

TBD

## Getting Started

1. Install dependencies (Yocto requirements, KAS, just)
2. Build your desired variant and stream
3. Flash the resulting image to an SD card
4. Insert the SD card into your Raspberry Pi Zero 2 W

Please note that additional hardware is required for full functionality.  \
More information will be added here at a later date.
